$(function() {

    //initialize background slideshow
    $.vegas('slideshow', {
        delay: 5000,
        fade: 1000,
        backgrounds:[
            { src:'images/01.jpg' },
            { src:'images/02.jpg' }
        ]
    });
});